package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = (width - text.trim().length());
			return  " ".repeat(extra) + text.trim();
		}

		public String flushLeft(String text, int width) {
			int extra = (width - text.trim().length());
			return  text.trim() + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			text = text.trim();
			String[] words = text.split(" ");
			List<String> wordlist = Arrays.asList(words);
			List<>
			for (String word : wordlist){}
			int spaces = words.length - 1;
			if(words.length == 1){
				return center(text, width);
			}



			return null;
		}};
		
	@Test
	void test() {


		
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
}
